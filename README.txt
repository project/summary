summary.module
README.txt

NOTE: this module has been made obsolete by views
(http://drupal.org/project/views). Views does all summary ever did and
much more. Summary is unlikely to be updated for newer versions of
Drupal.

Description
------------

The summary module provides several options for content display
on a home page in addition to the standard list of recent posts.
Summary implements functionality of, and therefore works best with, 
two other modules: node_image (for displaying images associated
with particular nodes and taxonomy terms--though at the time of 
writing this module has not yet been updated to 4.5) and 
taxonomy_context (for displaying info on taxonomies as site 
sections).

Summary also includes a block for recent postings, called "Latest 
postings".

Configuration
-------------
 
To configure the module, click administration > configuration > modules
> summary.  

The configuration options enable you to designate the following for
display on the summary page:

* Descriptive text 
* An image (floats right of descriptive text)
* If taxonomy_context is enabled, a list of top-level terms for
  a particular vocabulary.  If you use this option, it works best
  if you put your main content under one vocabulary.  For example, 
  you might have a "Topics" vocabulary with first-level terms
  "About", "Products", and "Downloads".  The summary module will then 
  display summaries about and links to each of these main sections.
* Whether to display the recent postings.

There are also settings to customize display of the "Latest postings"
block (including which types of nodes to display).

